package week1.exceptions;

public class MultipleExceptions {
    public static void main(String[] args) {
        try {
            someMethodThrowArrayIndexOutOfBoundsExceptions();
            toDivideThrowMyMathException(100, 0);
            simpleThrowRuntimeException();

            //1 вариант - общий родитель (одинаковая обработка (в нашем случае одинаковый текст ошибки))
            //instance of - как запасной вариант решения проблемы

            //2 вариант - множественная записись исключений (catch (E1 | E2 ...))
            //3 вариант - несколько catch блоков

        } catch (MyMathException e1) {
            System.err.println("LOG: Деление на 0.");
            //какие-то действия
        } catch (ArrayIndexOutOfBoundsException e2) {
            System.err.println("LOG: Проблемы с массивом");
        } catch (RuntimeException e3) {
            System.err.println("LOG: Рантайм исключение.");
            //какие-то действия

        }

    }

    public static void toDivideThrowMyMathException(int a, int b) throws MyMathException {
        try {
            System.out.println(a / b);
        } catch (ArithmeticException e) {
            throw new MyMathException(e.getMessage());
        }
    }

    public static void someMethodThrowArrayIndexOutOfBoundsExceptions() {
        int [] arr = new int[10];
        System.out.println(arr[10]);
    }

    public static void simpleThrowRuntimeException() {
        throw new RuntimeException();
    }
}
