package week1.exceptions.task4.exceptions;

public class MyDivisionByZeroException extends MyBaseException {
    public MyDivisionByZeroException(String errorMessage) {
        super(errorMessage);
    }

    public MyDivisionByZeroException() {
        super("Недопустимо деление на 0.");
    }
}
