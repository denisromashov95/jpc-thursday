package week1.exceptions.task4.exceptions;

public class MyInputException extends MyBaseException {
    public MyInputException(String errorMessage) {
        super(errorMessage);
    }


    public MyInputException(Throwable throwable) {
        super(throwable);
        throwable.printStackTrace();
    }
    public MyInputException() {
        super("Неправильный формат ввода.");
        System.err.println();
    }
}
