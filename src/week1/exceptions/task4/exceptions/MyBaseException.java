package week1.exceptions.task4.exceptions;

public class MyBaseException extends RuntimeException {
    public MyBaseException(String errorMessage) {
        super(errorMessage);
        System.out.println("LOG: " + errorMessage);
    }

    public MyBaseException(Throwable throwable) {
        super(throwable);
    }
}
