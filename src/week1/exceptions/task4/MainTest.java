package week1.exceptions.task4;

public class MainTest {
    public static void main(String[] args) {
        Calc calc = new Calc();

        try {
            calc.input();
            calc.calculate();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }


    }
}
