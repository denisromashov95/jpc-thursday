package week1.exceptions.resources;

public class MyResource implements AutoCloseable {
    @Override
    public void close() throws Exception {
        System.out.println("CLOSED MY RESOURCE!");
    }

    public void printHello() {
        System.out.println("Hello!");
    }
}
