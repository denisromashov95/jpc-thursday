package week1.exceptions;

import java.io.IOException;

public class SampleExample2 {
    public static void main(String[] args) throws MyMathException {
            int result = division(55, 1);
            System.out.println(result);
    }

    public static int division(int a, int b) throws MyMathException {
        try {
            return a / b;
        } catch (ArithmeticException exception) {
            System.err.println("ArithmeticException -> FOUND!");
            throw new MyMathException("Произошло деление на 0!", exception);
        } finally {
            System.out.println("Hello from finally!");
        }
    }
}
