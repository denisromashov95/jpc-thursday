package week3.reflection.task4;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/*
    Сконструировать класс. То есть вызвать конструктор класса и обработать все возможные ошибки.
 */
public class Main {
    public static void main(String[] args) {
        Class<Task4> clazz = Task4.class;

        try {
            Constructor<Task4>  constructor = clazz.getDeclaredConstructor(int.class, String.class);
            Task4 result = constructor.newInstance(142, "abc");

            System.out.println(result);
            System.out.println(result.a);
            System.out.println(result.b);

        } catch (NoSuchMethodException ex) {
            System.out.println("No such method...");
        } catch (IllegalAccessException ex) {
            System.out.println("The method access modifiers forbid calling it");
        } catch (InvocationTargetException ex) {
            System.out.println("The method has throw an exception");
        } catch (InstantiationException ex) {
            System.out.println("Class is abstract");
        }
    }
}

