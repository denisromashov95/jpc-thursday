package week3.annotation.task7;

public class Main {
    public static void main(String[] args) {
        SomeClassWithMethods p = new SomeClassWithMethods();

        LoggingService.log(p, "foo");
        p.foo(1);

        LoggingService.log(p, "bar");
        p.bar(2);

        LoggingService.log(p, "baz");
        p.baz(3);
    }
}
