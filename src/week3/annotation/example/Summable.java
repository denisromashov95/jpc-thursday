package week3.annotation.example;

public interface Summable {
    int sum(int a, int b);
}
