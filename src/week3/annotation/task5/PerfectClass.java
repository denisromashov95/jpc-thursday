package week3.annotation.task5;


import week3.annotation.task5.ClassDescription;

@ClassDescription(
        author = "Denis",
        date = "16.03.2023",
        currentRevision = 10,
        reviewers = {"Denis", "Andrey"}
)
public class PerfectClass {
}
