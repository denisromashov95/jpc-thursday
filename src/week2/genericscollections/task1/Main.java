package week2.genericscollections.task1;

/*
Создать класс Pair, который умеет хранить два значения:
1) Одинакового типа
2) Любого типа (T, V)
3) строка и число только
 */
public class Main {
    public static void main(String[] args) {
//        Pair<Double, String> pair = new Pair<>();
//        pair.key = 1d;
//        pair.value = "Значение";
//        pair.print();

        Pair<Double, String> pair = new Pair<>(1d, "апельсин");
        Pair<Double, String> pair2 = new Pair<>(1d, "яблоко");
        Pair<Double, String> pair3 = new Pair<>(1d, "апельсин");

        System.out.println(Pair.compare(pair, pair3));

    }
}
