package week2.genericscollections.task7;

import java.util.*;

/*
Простая задача: сохранить в мапе три элемента (1, “first”).
Вывести элемент значение по ключу 2
 */
public class MapSave {
    public static void main(String[] args) {
        Map<Integer, String> hashMap = new HashMap<>();
        hashMap.put(1, "first");
        hashMap.put(5, "first");
        hashMap.put(2, "second");
        hashMap.put(3, "third");
        hashMap.put(8, "OOOPS!");

        System.out.println("MAP Elements: " + hashMap);
        String s = hashMap.get(2);

//        System.out.println(hashMap.getOrDefault(99, "asd"));


        Collection<String> values = new ArrayList<>(hashMap.values());
        System.out.println(values);
        System.out.println("Значение по ключу 2: " + s);

        String str = hashMap.get(100);

        System.out.println(hashMap.getOrDefault(99, "asd"));
        System.out.println(hashMap.getOrDefault(1, "asd"));

        System.out.println(str.length()); // NPE!!!!
    }
}
