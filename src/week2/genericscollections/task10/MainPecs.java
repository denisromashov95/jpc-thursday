package week2.genericscollections.task10;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class MainPecs {
    static class Item {

    }

    static class Book extends Item {
        String name;
    }

    static class Phone extends Item {
        String model;
    }

    //PECS
    //Producer Extents, Consumer Super
    public static void copy(List<? extends Item> src, List<? super Item> dst) {
        for (Item item : src) {
            dst.add(item);
        }
    }

        /*
    Books ---
              \
                ---> Item ---> Object //т.е. копировать можно от подкласса к классу родителю
              /
    Phone ---
     */

    public static void main(String[] args) {
        List<Book> books = new ArrayList<>();
        List<Phone> phones = new ArrayList<>();
        List<Item> items = new ArrayList<>();
        List<Object> objects = new ArrayList<>();

        List<? extends Number> numbers = new ArrayList<>();


//        copy(books, phones); // ошибка
//        copy(items, books); // ошибка

        copy(books, items);
        copy(phones, objects);
    }

}
