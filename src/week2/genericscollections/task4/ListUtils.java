package week2.genericscollections.task4;

import java.util.List;

public class ListUtils {
    private ListUtils() {
    }

    public static <T> int countIf(List<T> elements, T element) {
        int counter = 0;
        for (T elem : elements) {
            //Сравнить по значению
//            if (elem.equals(element)) {
//                ++counter;
//            }

            //Сравнение по ссылки!
            if (elem == element) {
                ++counter;
            }

        }
        return counter;
    }
}
