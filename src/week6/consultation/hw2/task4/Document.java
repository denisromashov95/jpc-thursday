package week6.consultation.hw2.task4;

/*
В некоторой организации хранятся документы (см. класс Document).
Сейчас все документы лежат в ArrayList, из-за чего поиск по id документа выполняется неэффективно.
Для оптимизации поиска по id, необходимо помочь сотрудникам перевести хранение документов из ArrayList в HashMap.
 */
public class Document {
    public int id;
    public String name;
    public int pageCount;
    
    @Override
    public String toString() {
        return "Document{" + "id=" + id + "}";
    }
    
}
