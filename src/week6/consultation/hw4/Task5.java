package week6.consultation.hw4;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Task5 {
    public static void main(String[] args) {
        List<String> list = List.of("abc", "def", "qqq");

        System.out.println(list
                .stream()
                .map(String::toUpperCase)
                .collect(Collectors.joining(", ")));

        System.out.println(Stream.of("abc", "def", "qqq").map(e -> e.toUpperCase()).collect(Collectors.joining(", ")));

        //Решение от Владислава
        System.out.println(List.of("abc", "def", "qqq")
                .stream()
                .map(String::toUpperCase)
                .collect(Collectors.joining(", ")));

        //Решение от Антона
        String str = String.join(", ", list.stream().map(String::toUpperCase).toList());
    }
}
