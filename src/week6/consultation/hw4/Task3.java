package week6.consultation.hw4;

/*
    На вход подается список строк. Необходимо вывести количество непустых строк в списке.
    Например, для List.of("abc", "", "", "def", "qqq") результат равен 3.
 */

import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class Task3 {
    public static void main(String[] args) {
        List<String> list = List.of("abc", "", "", "def", "qqq");

        System.out.println(calc(list));

        //Решение от Антона
        int count = list.stream()
                .filter(str -> str.length() == 0).
                toList().size();

        Long count2 =  Stream.of("abc", "", "", "def", "qqq")
                .filter(s -> !s.isEmpty()).count();

        //Решение от Романа
        int count3 = (int) list.stream().filter(str -> !str.isEmpty()).count();
    }

    private static long calc(List<String> list) {
        return list.stream()
                .filter(Objects::nonNull)
                .filter(Predicate.not(String::isBlank)).count();
    }
}
