package week6.consultation.hw1;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.ResolverStyle;
import java.util.Date;

public class DateExample {

    public static void main(String[] args) throws ParseException {
        String str = "30.02.2023";

        DateFormat df = new SimpleDateFormat("dd.MM.yyyy");
        Date date = df.parse(str);
        System.out.println(date);

        LocalDate date1 = LocalDate.parse(str, DateTimeFormatter.ofPattern("dd.MM.uuuu").withResolverStyle(ResolverStyle.STRICT));
        System.out.println(date1);

    }

}
