package week6.consultation.hw1.task1;

public class MyCheckedException extends Exception  {
    // extends RuntimeException

    public MyCheckedException(String message) {
        super(message);
    }
}
