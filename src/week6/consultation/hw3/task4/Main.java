package week6.consultation.hw3.task4;

import week6.consultation.hw3.task4.structure.Z;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
//        List<Class<?>> alInterfaces = getAllInterfaces(Z.class);
        List<Class<?>> alInterfaces = getAllInterfacesRec(Z.class);
        for (Class<?> value : alInterfaces) {
            System.out.println(value);
        }
    }


    public static List<Class<?>> getAllInterfaces(Class<?> cls) {
        List<Class<?>> interfaces = new ArrayList<>();
        while (cls != Object.class) {
            for (Class<?> anInterface : cls.getInterfaces()) {
                interfaces.add(anInterface);
                Class<?>[] arrayInterface = anInterface.getInterfaces();
                while (arrayInterface.length > 0) {
                    for (Class<?> elementInterface : arrayInterface) {
                        anInterface = elementInterface;
                        interfaces.add(anInterface);
                        arrayInterface = anInterface.getInterfaces();
                    }
                }
            }
            cls = cls.getSuperclass();
        }
        return interfaces;
    }

    public static List<Class<?>> getAllInterfacesRec(Class<?> clazz) {
        if (clazz == null) {
            return null;
        } else {
            List<Class<?>> interfaces = new ArrayList<>();
            getAllInterfacesParents(clazz, interfaces);
            return interfaces;
        }
    }

    private static void getAllInterfacesParents(Class<?> clazz, List<Class<?>> interfacesResult) {
        while (clazz != null) {
            Class<?> [] interfaces = clazz.getInterfaces();
            for (Class<?> anInterface : interfaces) {
                if (!interfacesResult.contains(anInterface)) {
                    interfacesResult.add(anInterface);
                    getAllInterfacesParents(anInterface, interfacesResult);
                }
            }
            clazz = clazz.getSuperclass();
        }
    }

}
