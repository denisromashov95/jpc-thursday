package week6.consultation.hw3.addTask2;

public class Main {
    public static void main(String[] args) {

        String s1 = "{()[]()}";
        String s2 = "{)(}";
        String s3 = "[}";
        String s4 = "[{(){}}][()]{}";

        System.out.println(s1 + " - " + scannerStr(s1));
        System.out.println(s2 + " - " + scannerStr(s2));
        System.out.println(s3 + " - " + scannerStr(s3));
        System.out.println(s4 + " - " + scannerStr(s4));
    }



    public static boolean check(String s) {
        if (s.length() == 0) {
            return true;
        }

        int countOfRound = 0;
        int countOfCurly = 0;
        int countOfSquare = 0;
        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);
            if ((ch == ')' && countOfRound == 0)
                    || (ch == '}' && countOfCurly == 0)
                    || (ch == ']' && countOfSquare == 0)) {
                return false;
            }

            switch (ch) {
                case '(' -> countOfRound++;
                case ')' -> countOfRound--;
                case '{' -> countOfCurly++;
                case '}' -> countOfCurly--;
                case '[' -> countOfSquare++;
                case ']' -> countOfSquare--;
            }
        }

        return countOfRound == 0 && countOfCurly == 0 && countOfSquare == 0;
    }

    public static boolean checkRegexp(String str) {
        int len1,len2;
        String tmp=str.replaceAll("[^()\\[\\]{}]","");
        do {
            len1 = tmp.length();
            tmp = tmp.replaceAll("\\(\\)", "");
            tmp = tmp.replaceAll("\\[\\]", "");
            tmp = tmp.replaceAll("\\{\\}", "");
            len2 = tmp.length();
        } while (len1 != len2);
        return (len2==0);
    }

    //Решение от Владислава Л.
    private static boolean scannerStr(String str) {
        while (str.contains("[]") || str.contains("{}") || str.contains("()")) {
            str = str.replaceAll("\\([\\w\\s]*\\)|\\{[\\w\\s]*\\}|\\[[\\w\\s]*\\]", "");
        }
        return str.isEmpty();
    }


}
