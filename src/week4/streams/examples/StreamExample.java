package week4.streams.examples;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamExample {
    public static void main(String[] args) throws IOException {
        Supplier<Event> supplier = () -> new Event(UUID.randomUUID(), LocalDateTime.now(), "");
        Event event = supplier.get();
        System.out.println("\n" + event);

        List<String> list = new ArrayList<String>();
        list.add("One");
        list.add("Two");
        list.add("Three");
        list.add("Four");
        list.add("Five");
        list.add("Six");
        list.add("Seven");
        list.add("Eight");
        list.add("Nine");
        list.add("Ten");

        List.of("Hello", "World").stream();

        list.stream().collect(Collectors.toList());



//        list.forEach();

        Stream stream = list.stream();




        Stream parallelStream = list.parallelStream();
        Stream emptyStream = Stream.empty();

        System.out.println();

        IntStream.of(50, 60, 70, 80, 90, 100, 110, 120)
                .filter(x -> x < 90) //50, 60, 70, 80
                .map(x -> x + 1000)   //50, 60, 70, 80 (+1000 для каждого элемента)
                .limit(3) //1050, 1060, 1070
                .forEach(System.out::println);

        Map<?,?> map = new HashMap<>();
        map.entrySet().stream();

//        Arrays.stream();

        Stream<String> lines = Files.lines(Paths.get("src/jpc/week4/streams/streams/examples/some.txt"));
        Stream<Path> list2 = Files.list(Paths.get("./"));
        Stream<Path> walk = Files.walk(Paths.get("./"), 3);

    }
}
