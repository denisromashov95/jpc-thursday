package week4.functional.task1;

public class Timer {
    public long timeNanoSeconds = 0;

    public void measureTime(Runnable runnable) {
        long startTime = System.nanoTime(); // 1 - текущее время
        runnable.run(); //2 - запуск и окончание подсчета
        timeNanoSeconds = System.nanoTime() - startTime;
    }

}
