package week4.functional;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class FunctionalInterfaceExample {
    public static void main(String[] args) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Создали функциональный интерфейс");
            }
        }).start();

        new Thread(() -> System.out.println("Создали функциональный интерфейс через лямбду")).start();

        Runnable runnable = () -> System.out.println("Создали функциональный интерфейс через лямбду");
        runnable.run();

        Consumer<?> consumer;
        Predicate<?> predicate;
        Supplier<?> supplier;
        Function<?,?> function;

    }
}
