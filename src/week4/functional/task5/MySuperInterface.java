package week4.functional.task5;

@FunctionalInterface
public interface MySuperInterface<T> {
    T func(T value);
}
